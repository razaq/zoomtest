﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch
{
    [ElasticType(IdProperty = "Id", Name = "blog_post")]
    public class BlogPost
    {
        [ElasticProperty(Name = "id", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public Guid? Id { get; set; }

        [ElasticProperty(Name = "title", Index = FieldIndexOption.Analyzed, Type = FieldType.String)]
        public string Title { get; set; }

        [ElasticProperty(Name = "body", Index = FieldIndexOption.Analyzed, Type = FieldType.String)]
        public string Body { get; set; }

        /*public override string ToString()
        {
            return string.Format("Id: '{0}', Title: '{1}', Body: '{2}'", Id, Title, Body);
        }*/
    }
}
