﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch
{
    public class Commits
    {
        [JsonProperty("revision_cache")]
        public Commit Changeset { get; set; }
    }
}
