﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace ElasticSearch
{  
    [ElasticType(Name="pKeyRepoID", IdProperty="p_key")]
    class JiraToArtemis
    {   
        [ElasticProperty(Name="p_key", Index=FieldIndexOption.NotAnalyzed, Type=FieldType.String)]
        public string project_key { get; set; }
        
        [ElasticProperty(Name="p_name", Index=FieldIndexOption.NotAnalyzed, Type=FieldType.String)]
        public string project_name { get; set; }
        
        [ElasticProperty(Name="repo_id", Index=FieldIndexOption.NotAnalyzed, Type=FieldType.String)]
        public string repo_id { get; set; }
    }
}
