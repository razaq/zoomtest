﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace ElasticSearch
{
    [ElasticType(Name="tasks", IdProperty="Id")]
    class TaskFeed
    {
        [ElasticProperty(Name="id", Index=FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string Id { get; set; }

        [ElasticProperty(Name = "email", Index = FieldIndexOption.NotAnalyzed, Type=FieldType.String)]
        public string user_email { get; set; }

        [ElasticProperty(Name = "username", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string user_name { get; set; }

        [ElasticProperty(Name = "avatar", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string user_avatar { get; set; }

        [ElasticProperty(Name = "projectKey", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string project_key { get; set; }

        [ElasticProperty(Name = "projectName", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string project_name { get; set; }

        [ElasticProperty(Name = "projectCategory", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string project_Category { get; set; }

        [ElasticProperty(Name = "taskStatus", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string task_status { get; set; }

        [ElasticProperty(Name = "status", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string status { get; set; }

        [ElasticProperty(Name = "summary", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string summary { get; set; }

        [ElasticProperty(Name = "createdTime", Index = FieldIndexOption.NotAnalyzed)]
        public string created { get; set; }

        [ElasticProperty(Name = "updatedTime", Index = FieldIndexOption.NotAnalyzed)]
        public string updated { get; set; }
    }
}
