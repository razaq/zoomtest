﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch
{
    [ElasticType(IdProperty = "revision", Name = "commits")]
    public class Commit
    {
        /*[ElasticProperty(Name = "id", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string id { get; set; }*/

        [ElasticProperty(Name = "author", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        [JsonProperty("author")]
        public string author { get; set; }

        [ElasticProperty(Name = "repoID", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        [JsonProperty("repository_id")]
        public string repo_id { get; set; }

        [ElasticProperty(Name = "revision", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        [JsonProperty("revision")]
        public string revision { get; set; }

        [ElasticProperty(Name = "email", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        [JsonProperty("email")]
        public string email { get; set; }
        
        [ElasticProperty(Name = "time", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        [JsonProperty("time")]
        public string Time { get; set; }
    }
}
